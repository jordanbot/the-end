# The End

---

**TL;DR**:  
JordanBot will be decommissioned some time in the morning on the 20th of
July, 2018 (AEST).  
Please find another bot to replace JordanBot by this time.

---

JordanBot first started out a few years ago as a little extension of myself in
the Apink server - It would add emojis to messages, as well as respond with
`[citation needed]` to any potentially dubious claims made by other users.

Over time, this little community grew - as did the bot's features.  
We tinkered with music playback, profanity detection, and raid protection
measures (often against itself).

This would not have been possible without the amazing team that's been a part of
the project in some way or another over the years:

  * Attila - For contributing the occasional bit of code;
  * MicaLovesKPOP - For contributing many ideas and feature suggestions;
  * Sekl - For providing some sage advice about working with the Discord API;

Our team of testers:

  * dragonjuni;
  * Grand_A;
  * icecreamsmart;
  * Ivan;

The Admins, Owners, and fellow Moderators on the Apink server for (somewhat)
allowing these shenanigans, including:

  * Berk;
  * God Rong;
  * Ricky;
  * Chimes;

And last but not least - The numerous community members that have come and gone,
giving us their ideas, feedback, and company in the Discord server.

It fills me with sadness to say goodbye to JordanBot, not made easier by the
realisation that I no longer have enough time to both maintain and update
JordanBot.

---

**So what's going to happen?**

On the 20th of July (AEST), JordanBot will be shut down, and will disappear from
all the servers. Please use this time to arrange for an alternative bot in your
server.

The [JordanBot server](https://discord.gg/b4e63CD) will continue to operate as
a small (albeit quiet) social server.

There are other Discord-related projects going on in the background, and when
they are ready to see the light of day, they will be shared.

I _may_ look at running some kind of minimal version (aka JordanBot Light™)
purely inside the JordanBot server in the future, but for now it is all being
shut down.

---

We've learnt a lot, and had some great laughs along the way.

Thank you all for your feedback, support, and company - The things that made
working on this bot the most fun and rewarding.

-- _Jordan_
